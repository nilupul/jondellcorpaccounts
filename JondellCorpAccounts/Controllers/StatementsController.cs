using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using JondellCorpAccounts.Data;
using JondellCorpAccounts.Models;
using System.Globalization;
namespace JondellCorpAccounts.Controllers
{

    [Route("api/[controller]")]
    public class StatementsController : Controller
    {

        private readonly AccountsContext _context;

        public StatementsController(AccountsContext context)
        {
            _context = context;
        }

        // GET: api/statements
        [HttpGet]
        public async Task<Statement[]> Get()
        {
            //TODO error handling
            var statements = await _context.Statements
            .Include(s => s.AccountBalances)
            .OrderBy(d => d.StatementDate)
            .ToArrayAsync();
            return statements;
        }

        // GET: api/statements/getbydate
        [HttpGet("[action]")]
        public async Task<Statement> GetByDate(string month, string year)
        {
            //TODO validate mandatory fields
            //TODO error handling

            var statement = await _context.Statements
                      .SingleOrDefaultAsync(m => m.StatementDate == DateTime.ParseExact(month + "-" + year, "MMMM-yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None));
            _context.Entry(statement).Collection(p => p.AccountBalances).Load();

            return statement;
        }

        // GET: api/statements/downloadsample
        [HttpGet("[action]")]
        public FileResult DownloadSample()
        {
            //TODO error handling
            HttpContext.Response.ContentType = "application/text";
            FileContentResult result = new FileContentResult(System.IO.File.ReadAllBytes("Content/Sample.txt"), "application/text")
            {
                FileDownloadName = "Sample.txt"
            };
            return result;
        }

        // POST: api/statements/uploadfile
        [HttpPost("[action]")]
        public Statement UploadFile(IFormFile file)
        {

            //TODO validate mandatory fields
            //TODO error handling
            //TODO file validation
            string month = Request.Headers["month"];
            string year = Request.Headers["year"];
            var linesList = new List<string>();
            using (StreamReader reader = new StreamReader(file.OpenReadStream()))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    linesList.Add(line);
                }
            }
            Statement statement = new Statement { StatementDate = DateTime.ParseExact(month + "-" + year, "MMMM-yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None) };
            _context.Statements.Add(statement);
            _context.SaveChanges();

            var accountBalances = new List<AccountBalance>();

            for (int i = 1; i < linesList.Count; i++)
            {
                string[] strArray = linesList[i].Split('\t');
                AccountBalance acc = new AccountBalance
                {
                    Name = strArray[0],
                    Balance = double.Parse(strArray[1]),
                    StatementID = statement.StatementID
                };
                accountBalances.Add(acc);
            }

            foreach (AccountBalance accountBalance in accountBalances)
            {
                _context.AccountBalances.Add(accountBalance);
            }
            _context.SaveChanges();

            return statement;
        }
    }
}
