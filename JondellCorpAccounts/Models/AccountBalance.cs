using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace JondellCorpAccounts.Models
{
    public class AccountBalance
    {
        public int AccountBalanceID { get; set; }
        public string Name { get; set; }
        public double Balance { get; set; }
        public int StatementID { get; set; }
    }
}
