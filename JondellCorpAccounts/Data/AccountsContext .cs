﻿using JondellCorpAccounts.Models;
using Microsoft.EntityFrameworkCore;

namespace JondellCorpAccounts.Data
{
    public class AccountsContext : DbContext
    {
        public AccountsContext(DbContextOptions<AccountsContext> options) : base(options)
        {
        }

        public DbSet<Statement> Statements { get; set; }
        public DbSet<AccountBalance> AccountBalances { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Statement>().ToTable("Statement");
            modelBuilder.Entity<AccountBalance>().ToTable("AccountBalance");
        }
    }
}