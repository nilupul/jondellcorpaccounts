﻿using JondellCorpAccounts.Models;
using System;
using System.Linq;
using System.Globalization;
using System.Collections.Generic;

namespace JondellCorpAccounts.Data
{
    public static class DbInitializer
    {
        public static void Initialize(AccountsContext context)
        {
            context.Database.EnsureCreated();

            // Look for any students.
            if (context.Statements.Any())
            {
                return;   // DB has been seeded
            }

            var statement = new Statement { StatementDate = DateTime.ParseExact("January-2017", "MMMM-yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None) };

            context.Statements.Add(statement);

            context.SaveChanges();
        }
    }
}