import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UpdateBalanceComponent } from './updatebalance.component';
import { Http } from '@angular/http';
import {UpdateBalanceRoutingModule} from "./updatebalance-routing.module";
import { FileUploadComponent } from './../shared/components/file-upload/file-upload.component';
import { FileUploadModule } from "ng2-file-upload";
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    UpdateBalanceRoutingModule,
      FileUploadModule,
      FormsModule
  ],
  declarations: [UpdateBalanceComponent,FileUploadComponent]
})
export class UpdateBalanceModule { }
