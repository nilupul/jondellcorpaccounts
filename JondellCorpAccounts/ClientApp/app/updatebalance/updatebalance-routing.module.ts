import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UpdateBalanceComponent} from "./updatebalance.component";

const routes: Routes = [
  {
    path: '',
    component: UpdateBalanceComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UpdateBalanceRoutingModule { }

