import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewBalanceComponent } from './viewbalance/viewbalance.component';
import { UpdateBalanceComponent } from './updatebalance/updatebalance.component';
import { ReportsComponent } from './reports/reports.component';



export const routes: Routes = [

    //TODO Lazy loading modules isn't working at the moment hence importing the component here
    // {
    //    path: 'view-balance', loadChildren: './viewbalance/viewbalance.module#ViewModule'
    // },
    {
        path: 'view-balances', component: ViewBalanceComponent,
    },
    {
        path: 'update-balances', component: UpdateBalanceComponent,
    },
    {
        path: 'reports', component: ReportsComponent,
    },

    {
        path: '', redirectTo: 'view-balances', pathMatch: 'full'
    },
    {
        path: '**', redirectTo: 'view-balances'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
