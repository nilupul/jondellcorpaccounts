import { Component } from '@angular/core';
import { Http } from '@angular/http';

@Component({
    selector: 'viewbalance',
    templateUrl: './viewbalance.html'
})
export class ViewBalanceComponent {
    public statement: Statement;
    public months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    public years = ["2017", "2018", "2019"];
    public selectedYear = this.years[0];
    public selectedMonth = this.months[0];;
    public http;
    public format = { month: 'long', year: 'numeric' };

    constructor(http: Http) {
        this.http = http;
    }

    onChange(city) {
        this.http.get('api/statements/GetByDate?month=' + this.selectedMonth + '&year=' + this.selectedYear).subscribe((result) => {
            this.statement = result.json() as Statement;
            this.statement.statementDate = new Date(this.statement.statementDate).toLocaleDateString('en-US', this.format);
        },
        (error) => {
            alert("Balance information is not available for the selcted date!");
            this.statement = null;
        });
    }
}

interface AccountBalance {
    name: string;
    balance: number;
}
interface Statement {
    statementDate: string;

    accountBalances: AccountBalance[];
    month: string;
    year: string;
}
