import { BrowserModule } from '@angular/platform-browser';
import { NgModule ,Directive} from '@angular/core';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { NavMenuModule } from './shared/components/navmenu/navmenu.module';
import { FileUploadComponent } from './shared/components/file-upload/file-upload.component';
import { ViewBalanceComponent } from './viewbalance/viewbalance.component';
import { UpdateBalanceComponent } from './updatebalance/updatebalance.component';
import { ReportsComponent } from './reports/reports.component';
import { ChartsModule } from 'ng2-charts';
import { FileSelectDirective } from 'ng2-file-upload/ng2-file-upload';

@NgModule({
    declarations: [
        AppComponent,
        ViewBalanceComponent,
        UpdateBalanceComponent,
        ReportsComponent,
        FileSelectDirective,
        FileUploadComponent
    ],
    imports: [
        BrowserModule,
        HttpModule,
        NavMenuModule,
        ChartsModule,
        FormsModule,
        AppRoutingModule

    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModuleShared { }
