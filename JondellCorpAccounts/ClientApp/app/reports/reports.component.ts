import { Component } from '@angular/core';
import { Http } from '@angular/http';
@Component({
    selector: 'reports',
    templateUrl: './reports.html'
})
export class ReportsComponent {
    // lineChart

    public lineChartData: Array<any> = [];
    public lineChartLabels: Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
    public lineChartOptions: any = {
        responsive: true
    };
    public format = { month: 'long', year: 'numeric' };
    public lineChartColors: Array<any> = [
        { // grey
            backgroundColor: 'rgba(148,159,177,0.2)',
            borderColor: 'rgba(148,159,177,1)',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        },
        { // dark grey
            backgroundColor: 'rgba(77,83,96,0.2)',
            borderColor: 'rgba(77,83,96,1)',
            pointBackgroundColor: 'rgba(77,83,96,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(77,83,96,1)'
        },
        { // grey
            backgroundColor: 'rgba(148,159,177,0.2)',
            borderColor: 'rgba(148,159,177,1)',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        }
    ];
    public lineChartLegend: boolean = true;
    public lineChartType: string = 'line';
    public http;

    constructor(http: Http) {
        this.http = http;
        this.populate();

    }

    public populate(): void {

        this.http.get('api/statements/').subscribe(result => {
              //TODO if a new account is added, data will be added into the 0 index of the lineChartData
              //This is a critical bug 
              let statements = result.json() as Statement[];
              let lineChartDataUpdated: Array<any> = [];
              let lineChartLabelsUpdated: Array<any> = [];
              statements.forEach((statement) => {

                  lineChartLabelsUpdated.push(new Date(statement.statementDate).toLocaleDateString('en-US', this.format));

                  statement.accountBalances.forEach((accountBalance) => {

                      if (!lineChartDataUpdated.some(x => x.label === accountBalance.name)) {
                          lineChartDataUpdated.push({ data: [accountBalance.balance], label: accountBalance.name });

                      } else {
                          let itemIndex = lineChartDataUpdated.findIndex(item => item.label == accountBalance.name);
                          lineChartDataUpdated[itemIndex].data.push(accountBalance.balance);
                      }
                  });
              });
              this.lineChartData = lineChartDataUpdated;
              this.lineChartLabels = lineChartLabelsUpdated;
        });

    }
}

interface AccountBalance {
    name: string;
    balance: number;
}
interface Statement {
    statementDate: string;

    accountBalances: AccountBalance[];
    month: string;
    year: string;
}
