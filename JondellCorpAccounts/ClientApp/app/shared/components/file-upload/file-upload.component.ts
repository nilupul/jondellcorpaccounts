import { Component } from '@angular/core';
import { FileSelectDirective, FileDropDirective, FileUploader } from 'ng2-file-upload/ng2-file-upload';
const URL = '/api/statements/UploadFile';


@Component({
  selector: 'wf-file-upload',
  templateUrl: './file-upload.html'
})
export class FileUploadComponent {

  public hasBaseDropZoneOver:boolean = false;
  public hasAnotherDropZoneOver:boolean = false;
  public months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  public years = ["2017", "2018", "2019"];
  public selectedYear = this.years[0];
  public selectedMonth = this.months[0];;

  public uploader:FileUploader;
  constructor() {

    this.uploader = new FileUploader({
        url: URL,
        allowedMimeType: ['text/plain'],
        maxFileSize: 5 * 1024 * 1024
    });
    this.uploader.onAfterAddingFile = f => {
        
    };
    this.uploader.onBeforeUploadItem = (item) => {
        this.uploader.setOptions({
            headers: [
                { name: "month", value: this.selectedMonth },
                { name: "year", value: this.selectedYear }]
        })
    }

    this.uploader.onCompleteItem = (item:any, response:any, status:any, headers:any) => {
        alert("You've uploaded the file successfully!");
      this.uploader.clearQueue();
    };

   }
  public fileOverBase(e:any):void {
    this.hasBaseDropZoneOver = e;
  }
 
  public fileOverAnother(e:any):void {
    this.hasAnotherDropZoneOver = e;
  }
}

