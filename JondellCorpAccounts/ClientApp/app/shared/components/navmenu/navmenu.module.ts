import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NavMenuComponent} from './navmenu.component';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  entryComponents: [NavMenuComponent],
  declarations: [NavMenuComponent],
  exports: [NavMenuComponent]
})
export class NavMenuModule { }
