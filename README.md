# Jondell Corp Accounts App

This is an Angular 2 / ASP.NET Core MVC web application which uses Entity Framework with MSSQL database.
Azure deployment can be found at: http://jondellcorpaccounts.azurewebsites.net

## Prerequisites
    Node.js 
    .NET Core

## Development environment run

Run `npm install` to install application’s dependencies
Run `webpack --config webpack.config.vendor.js` to bundle third party dependancies
Run `webpack --config webpack.config.js` to bundle the code
Run `dotnet run` to run

## Running unit tests

Run `npm test` to execute the unit tests via [Karma](https://karma-runner.github.io).